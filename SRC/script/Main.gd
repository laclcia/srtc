extends Node2D
func _ready() -> void:
	get_tree().get_root().set_transparent_background(true)
	OS.window_per_pixel_transparency_enabled = true
func _input(_event: InputEvent) -> void:
	match Input.is_action_just_pressed("ui_exit"):
		true:
			get_tree().quit()
	match Input.is_action_just_pressed("t_transparent"):
		true:
			OS.window_per_pixel_transparency_enabled = !OS.window_per_pixel_transparency_enabled
			$CanvasLayer/ColorRect.visible = !$CanvasLayer/ColorRect.visible
func _process(_delta: float) -> void:
	var timeDict = OS.get_time()
	var hour = str(timeDict.hour)
	var minute = str(timeDict.minute)
	var seconds = str(timeDict.second)
	hour = _ajust_zero(hour)
	minute = _ajust_zero(minute)
	seconds  = _ajust_zero(seconds)
	$hours.bbcode_text = str(hour)
	$min.bbcode_text = str(minute)
	$sec.bbcode_text = str(seconds)
func _on_Button_pressed() -> void:
	get_tree().quit()
func _ajust_zero(number):
	match number:
		"0":
			number = "00"
		"1":
			number = "01"
		"2":
			number = "02"
		"3":
			number = "03"
		"4":
			number = "04"
		"5":
			number = "05"
		"6":
			number = "06"
		"7":
			number = "07"
		"8":
			number = "08"
		"9":
			number = "09"
		_:
			pass
	return number
func _on_ALLWAYS_ON_TOP_pressed() -> void:
	$CanvasLayer/ColorRect.visible = !$CanvasLayer/ColorRect.visible
