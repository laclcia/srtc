# SRTC

a small real time clock project in godot for convinience.


## Installation
download lastest release from here\
https://gitlab.com/laclcia/srtc/-/raw/main/SRC/build/SmollRTC.x86_64 \
and set executable "chmod +x ./SmollRTC.X86_64"

## Usage
run the binary either from cmd or double clikcing on it.\
ESC to quit the software while it is in focus\
T to toggle background transparency (default transparent)

## Contributors
    Laclcia "coding ,developping and UI design" 
    Kevin Covollo "art" 
    Irwin Vega "art"

## License
MIT

## Project status
In developpement.

